Document: nted-fr
Title: NtEd documentation, French translation
Author: Jörg Anders, Willy Baré
Abstract: Full documentation for the musical score editor NtEd.
Section: Sound

Format: HTML
Index: /usr/share/doc/nted/fr/index.html
Files: /usr/share/doc/nted/fr/*.html
